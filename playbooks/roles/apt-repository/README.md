apt-repository
============================

Author
------------
(c) SAV at 2019

Apt repository installation.

Requirements
------------

None.

Role Variables
--------------

* `apt_repository_repositories` is an array of key-pairs `repo`-`key`.

Dependencies
------------

None.

Example Playbook
----------------

    ---
    - hosts: localhost
      become: true
      roles:
        - role: apt-repository
          apt_repository_repositories:
            - repo: deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main
              key: https://www.postgresql.org/media/keys/ACCC4CF8.asc
            - repo: deb http://nginx.org/packages/mainline/ubuntu/ trusty nginx
              key: http://nginx.org/keys/nginx_signing.key
            - repo: ppa:lesovsky/pgcenter

License
-------

Private
